class PatientDto {
    getModel(body) {
        let patientID = body.patientID;
        let fullName = body.fullName;
        let age = body.age;
        let vaccine = body.vaccine;
        let firstDose = body.firstDose;
        let secondDose = body.secondDose;
        let firstDoseDate = body.firstDoseDate;
        let secondDoseDate = body.secondDoseDate;

        return {
            patientID: patientID,
            fullName: fullName,
            age: age,
            vaccine: vaccine,
            firstDose: firstDose,
            secondDose: secondDose,
            firstDoseDate: firstDoseDate,
            secondDoseDate: secondDoseDate
        };
    }
}

module.exports = PatientDto;