class VaccineDto {
    getModel(body) {
        let name = body.name;
        let numberOfDoses = body.numberOfDoses;
        let timeBetweenDoses = body.timeBetweenDoses;
        return {
            name: name,
            numberOfDoses: numberOfDoses,
            timeBetweenDoses: timeBetweenDoses
        };
    }
}

module.exports = VaccineDto;