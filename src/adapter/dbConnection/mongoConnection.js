const MongoClient = require('mongodb').MongoClient;

class MongoDBConnection {
    constructor() {
        this.client = null;
        this.dataBase = null;
    }

    connect(url) {
        return new Promise(function (resolve, reject) {
            MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true},
                (error, client) => {
                    if (error)
                        reject(error);
                    else {
                        console.log("Connected to MongoDB");
                        resolve(client);
                    }
                });
        })
    }

    obtainCollection(name) {
        return this.dataBase.collection(name);
    }

    closeConnection() {
        console.log('Closing connection...');
        this.client.close();
    }

    getOne(searchCriteria, collection) {
        return new Promise(function (resolve, reject) {
            collection.find(searchCriteria).toArray(function (error, results) {
                if (error)
                    reject(error)
                else
                    resolve(results);
            })
        })
    }

    getAll(collection) {
        return new Promise(function (resolve, reject) {
            collection.find({}).toArray(function (error, results) {
                if (error)
                    reject(error);
                else
                    resolve(results);
            })
        })
    }

    insert(objectToInsert, collection) {
        return new Promise(function (resolve, reject) {
            collection.insertOne(objectToInsert, function (error, resp) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(resp);
                }
            })
        })
    }

    insertMany(objectsToInsert, collection) {
        return new Promise(function (resolve, reject) {
            collection.insertMany(objectsToInsert, collection, function (error, resp) {
                if (error)
                    reject(error);
                else
                    resolve(resp);
            });
        })
    }

    update(searchCriteria, updatedObject, collection) {
        return new Promise(function (resolve, reject) {
            collection.updateOne(searchCriteria, updatedObject, function (error, resp) {
                if (error) {
                    reject(error);
                }
                else
                    resolve(resp);
            })
        })
    }

    filterByID(filterCriteria, collection) {
        return new Promise(function (resolve, reject) {
            collection.find(filterCriteria).toArray(function (error, results){
                if (error) {
                    console.log("Error on find by input");
                    reject(error);
                } else {
                    console.log("Filtered results: ", results);
                    resolve(results);
                }
            })
        })
    }

    delete(searchCriteria, collection) {
        return new Promise(function (resolve, reject) {
            collection.deleteOne(searchCriteria, function (error, resp) {
                if (error)
                    reject(error);
                else
                    resolve(resp);
            });
        });
    }
}

module.exports = MongoDBConnection;