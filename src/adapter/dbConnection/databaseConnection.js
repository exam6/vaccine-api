const env = require('../../config/enviromentVariables');
const mongoDBConnection = require('./mongoConnection');


class DBConnection {
    constructor() {
    }

    static async connectDB(){
        const mongo = new mongoDBConnection();
        try {
            mongo.client = await mongo.connect(env.mongoUri)
                .catch(error => {
                    console.log("ERROR trying connection: ", error);
                });
            mongo.dataBase = mongo.client.db(env.mongoDBName);
            return mongo;
        } catch (e) {
            console.log(e);
        }
    }
}

module.exports = DBConnection;
