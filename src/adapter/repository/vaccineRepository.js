const mongo = require('mongodb');

class VaccineRepository {
    constructor(dbConnection) {
        this.dbConnection = dbConnection;
        this.collection = this.dbConnection.obtainCollection('vaccine');
    }

    async getOne(id) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = { '_id': o_id };
        return await this.dbConnection.getOne(searchCriteria, this.collection)
            .catch(error => {
                console.log("Error in getOne on Vaccine Repository: ", error);
                return {success: false, message: error.toString()};
            });
    }

    async getByName(vaccineName) {
        let query = {'name': vaccineName};
        return await this.dbConnection.getOne(query, this.collection)
            .catch(error => {
                console.log('Error in get by name on vaccine repository', error);
                return {success: false, message: error.toString()};
            });
    }

    async getAll() {
        return this.dbConnection.getAll(this.collection)
            .catch(error => {
                console.log("Error in getAll on  Vaccine Repository: ", error);
                return {success: false, message: error.toString()};
            });
    }

    async getAllBy(orderCriteria) {
        let list = await this.getAll();
        list.sort(function (a, b){
            switch (orderCriteria) {
                case 'name':
                    if (a.name > b.name) return 1;
                    if (a.name < b.name) return -1;
                    break;
                case 'numberOfDoses':
                    if (a.numberOfDoses > b.numberOfDoses) return 1;
                    if (a.numberOfDoses < b.numberOfDoses) return -1;
                    break;
                case 'timeBetweenDoses':
                    if (a.timeBetweenDoses > b.timeBetweenDoses) return 1;
                    if (a.timeBetweenDoses < b.timeBetweenDoses) return -1;
                    break;
            }
        });
        return list;
    }

    async insert(object,) {
        return this.dbConnection.insert(object, this.collection).then(() => {
            console.log("The vaccine was created successfully");
            return { success: true, message: "The vaccine was created successfully" };
        }).catch(error => {
            console.log("Error in insert on  Vaccine Repository: ", error);
            return {success: false, message: error.toString()};
        });
    }

    async update(id, object) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = {'_id': o_id};
        return this.dbConnection.update(searchCriteria, {$set: object}, this.collection).then(() => {
            console.log("The vaccine was updated successfully");
            return { success: true, message: "The vaccine was updated successfully" };
        }).catch(error => {
            console.log("Error in update on Vaccine Repository: ", error);
            return {success: false, message: error.toString()};
        });
    }


    async delete(id) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = {'_id': o_id};
        return this.dbConnection.delete(searchCriteria, this.collection).then(() => {
            console.log("The patient was deleted successfully");
            return { success: true, message: "The vaccine was deleted successfully" };
        }).catch(error => {
            console.log("Error in delete on Vaccine Repository: ", error);
            return {success: false, message: error.toString()};

        });
    }
}

module.exports = VaccineRepository;