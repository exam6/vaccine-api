const mongo = require('mongodb');

class PatientRepository {
    constructor(dbConnection) {
        this.dbConnection = dbConnection;
        this.collection = this.dbConnection.obtainCollection('patient');
    }

    async getOne(id) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = { '_id': o_id };
        return await this.dbConnection.getOne(searchCriteria, this.collection)
            .catch(error => {
                console.log("Error in getOne on PatientRepository: ", error);
                return {success: false, message: error.toString()};
            });
    }

    async getAll() {
        return this.dbConnection.getAll(this.collection)
            .catch(error => {
                console.log("Error in getAll on PatientRepository: ", error);
                return {success: false, message: error.toString()};
            });
    }

    async getAllBy(orderCriteria) {
        let list = await this.getAll();
        list.sort(function (a, b){
            switch (orderCriteria) {
                case 'name':
                    if (a.fullName > b.fullName) return 1;
                    if (a.fullName < b.fullName) return -1;
                    break;
                case 'vaccine':
                    if (a.vaccine > b.vaccine) return 1;
                    if (a.vaccine < b.vaccine) return -1;
                    break;
            }
        });
        return list;
    }


    async insert(object,) {
        return this.dbConnection.insert(object, this.collection).then(() => {
            console.log("The patient was created successfully");
            return { success: true, message: "The patient was created successfully" };
        }).catch(error => {
            console.log("Error in insert on PatientRepository: ", error);
            return {success: false, message: error.toString()};
        });
    }

    async update(id, object) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = {'_id': o_id};
        return this.dbConnection.update(searchCriteria, {$set: object}, this.collection).then(() => {
            console.log("The patient was updated successfully");
            return { success: true, message: "The patient was updated successfully" };
        }).catch(error => {
            console.log("Error in update on PatientRepository: ", error);
            return {success: false, message: error.toString()};
        });
    }

    async filterByID(input) {
        let str = { "patientID" : { "$regex": input } };
        return this.dbConnection.filterByID(str, this.collection)
            .catch(error => {
                console.log("Error on getting filtered docs by input: ", error);
                return {success: false, message: error.toString()};
            });
    }

    async delete(id) {
        let o_id;
        if (id)
            o_id = new mongo.ObjectId(id);
        let searchCriteria = {'_id': o_id};
        return this.dbConnection.delete(searchCriteria, this.collection).then(() => {
            console.log("The patient was deleted successfully");
            return { success: true, message: "The patient was deleted successfully" };
        }).catch(error => {
            console.log("Error in delete on PatientRepository: ", error);
            return {success: false, message: error.toString()};

        });
    }
}

module.exports = PatientRepository;