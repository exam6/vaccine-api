const express = require('express');
const router = express.Router();

const PatientController = require('../controller/patientController');
const VaccineController = require('../controller/vaccineController');

router.use('/patient', PatientController);
router.use('/vaccine', VaccineController);

module.exports = router;