const express = require('express');
const bodyParser = require('body-parser');

const cors = require('cors');
const http = require('http');

const serverPort = process.env.PORT || '4000';
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


app.set('port', serverPort);

const routes = require('./adapter/routes');
app.use('/api', routes);

let server = http.createServer(app);

server.listen(serverPort, function (){
    console.log('Running on port ' + serverPort);
});
