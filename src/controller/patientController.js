const express = require('express');
const router = express.Router();
let DatabaseConnection = require('../adapter/dbConnection/databaseConnection');

const PatientRepository = require('../adapter/repository/patientRepository');
const PatientDTO = require('../adapter/dto/patientDto');

let patientRepository;

async function connectDB() {
    let db = await DatabaseConnection.connectDB();
    patientRepository = new PatientRepository(db);
    return db;
}

router.get("", async function (request, response){
    let db = await connectDB();
    let patientList = await patientRepository.getAll()
        .catch(error => {
            console.log("Error getting patient list");
        });
    db.closeConnection();
    response.send(patientList);
});

router.get("/:id", async function (request, response) {
    let db = await connectDB();
    let id = request.params.id;
    let patient = await patientRepository.getOne(id)
        .catch(error => {
            console.log("ERROR getting a patient with id: "+ id + ": ", error);
        });
    db.closeConnection();
    response.send(patient);
});

router.get("/order/:criteria", async function (request, response){
    let db = await connectDB();
    let criteria = request.params.criteria;
    let list = await patientRepository.getAllBy(criteria)
        .catch(error => {
            console.log("Error getting ordered vaccine list: ", error);
        });
    db.closeConnection();
    response.send(list);
});

router.get("/filter/:input", async function (request, response) {
    let db = await connectDB();
    let input = request.params.input;
    let list = await patientRepository.filterByID(input)
        .catch(error => {
            console.log("Error on filter list by input");
        });
    db.closeConnection();
    response.send(list);
});

router.put("/:id", async function (request, response) {
    let db = await connectDB();
    let id = request.params.id;
    let patientDTO = new PatientDTO();
    let patientData = patientDTO.getModel(request.body);
    console.log(patientData);
    let putResponse  = {success:false,  message: 'ERROR updating a patient'};
    try {
        putResponse = await patientRepository.update(id, patientData);
    } catch (error) {
        console.log("ERROR updating a patient with id:" + id, error);
    }
    db.closeConnection();
    response.send(putResponse);
});

router.post("/", async function (request, response) {
    let db = await connectDB();
    let isInsertedPatient = false;
    try {
        isInsertedPatient = await patientRepository.insert(request.body);
    } catch (error) {
        console.log("ERROR registering a patient", error);
    }
    db.closeConnection();
    response.send(isInsertedPatient);
});

router.delete("/:id", async function (request, response) {
    let db = await connectDB();
    let id = request.params.id;
    let deleteResponse = false;
    try {
        deleteResponse = await patientRepository.delete(id);
    } catch (error) {
        console.log("ERROR deleting a patient with id: "+ id + ": ", error);
    }
    db.closeConnection();
    response.send(deleteResponse);
});

module.exports = router;