const express = require('express');
const router = express.Router();
let DatabaseConnection = require('../adapter/dbConnection/databaseConnection');

const VaccineRepository = require('../adapter/repository/vaccineRepository');
const VaccineDTO = require('../adapter/dto/vaccineDto');
let vaccineRepository;


async function connectDB() {
    let db = await DatabaseConnection.connectDB();
    vaccineRepository = new VaccineRepository(db);
    return db;
}

router.get("", async function (request, response){
    let db = await connectDB();
    let vaccineList = await vaccineRepository.getAll()
        .catch( error => {
            console.log("Error getting vaccine list");
        });
    db.closeConnection();
    response.send(vaccineList);
});

router.get("/:id", async function (request, response){
    let db = await connectDB();
    let id = request.params.id;
    let vaccine = await vaccineRepository.getOne(id)
        .catch(error => {
            console.log("Error getting vaccine with id: " + id + ": ", error);
        });
    db.closeConnection();
    response.send(vaccine);
});

router.get("/name/:name", async function (request, response){
    let db = await connectDB();
    let name = request.params.name;
    let vaccine = await vaccineRepository.getByName(name)
        .catch(error => {
            console.log("Error getting vaccine with name " + name +": ", error);
        });
    db.closeConnection();
    response.send(vaccine);
});

router.get("/orderBy/:criteria", async function (request, response){
    let db = await connectDB();
    let criteria = request.params.criteria;
    let list = await vaccineRepository.getAllBy(criteria)
        .catch(error => {
            console.log("Error getting ordered vaccine list: ", error);
        });
    db.closeConnection();
    response.send(list);
});

router.post("", async function (request, response){
    let db = await connectDB();
    let isRegistered = false;
    try {
        isRegistered = await vaccineRepository.insert(request.body);
    } catch (e) {
        console.log("ERROR on register vaccine");
    }
    db.closeConnection();
    response.send(isRegistered);
});

router.put("/:id", async function (request, response) {
    let db = await connectDB();
    let id = request.params.id;
    let vaccineDTO = new VaccineDTO();
    let vaccineData = vaccineDTO.getModel(request.body);
    let putResponse  = {success:false,  message: 'ERROR updating a vaccine'};
    try {
        putResponse = await vaccineRepository.update(id, vaccineData);
    } catch (error) {
        console.log("ERROR updating vaccine with id:" + id, error);
    }
    db.closeConnection();
    response.send(putResponse);
});

router.delete("/:id", async function (request, response){
    let db = await connectDB();
    let id = request.params.id;
    let deleteResponse = false;
    try {
        deleteResponse = await vaccineRepository.delete(id);
    } catch (error) {
        console.log("ERROR deleting vaccine with id: "+ id + ": ", error);
    }
    db.closeConnection();
    response.send(deleteResponse);
});

module.exports = router;